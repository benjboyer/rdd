# README #

rdd is an open-source project build in c# with the framework .net 4.5

### What is this repository for? ###

The main goal of rdd is to facilitate the implementation of Responsibility Design-Driven applications on the market.
Here's the wiki article for the topic: http://en.wikipedia.org/wiki/Responsibility-driven_design.

The actual version is **1.1.3**

### How do I get set up? ###

Clone the repository and you're ready to go.

### Contribution guidelines ###

You must do tests: Provide your unit tests addition or changes related to your work. Also, let's keep the naming conventions I established.

### Who do I talk to? ###

My name is Ben and I am the owner of this repository (obviously).

Have fun folks :)!