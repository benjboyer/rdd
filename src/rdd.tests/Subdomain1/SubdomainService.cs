﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.tests.Subdomain1
{
    internal class SubdomainService: IContract
    {
        #region Fields

        private readonly SubdomainParameter subdomainParameter;

        #endregion

        #region Properties

        public string PropertyOne
        {
            get
            {
                return this.propertyOne;
            }
        }
        private string propertyOne = string.Empty;

        #endregion

        #region Constructor

        [ImportingConstructor]
        public SubdomainService(
                SubdomainParameter subdomainParameter
            )
        {
            this.subdomainParameter = subdomainParameter;
        }

        #endregion

        #region Implementation of Subdomain1.IContract
        public void MethodOne(string message)
        {
            this.propertyOne = message + " " + subdomainParameter.Value;
        }

        #endregion
    }
}
