﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.tests.Subdomain1
{
    /// <summary>
    /// A simple contract defintion.
    /// This object is a "contract".
    /// </summary>
    [InheritedExport]
    public interface IContract
    {
        /// <summary>
        /// A simple property. The name tells this property has a knowing information stereotype.
        /// </summary>
        string PropertyOne { get; }

        /// <summary>
        /// A simple method. The name tells this methods has a doing information stereotype.
        /// This example will inject a message into an object implementor. The value will be returned by MyRoleInformationAsKnowledge.
        /// </summary>
        /// <param name="message">The message to inject.</param>
        void MethodOne(string message);
    }
}
