﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.tests.Subdomain1
{
    [Export]
    public class SubdomainParameter
    {
        #region Properties

        /// <summary>
        /// A custom value for the injection tool.
        /// </summary>
        public string Value
        {
            get
            {
                return _value;
            }
        }

        private readonly string _value;

        #endregion

        #region Constructor

        public SubdomainParameter(string value)
        {
            this._value = value;
        }

        #endregion
    }
}
