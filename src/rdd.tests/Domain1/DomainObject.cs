﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.tests.Domain1
{
    /// <summary>
    /// The domain structurer. In this case, this objects holds the access to service layer.
    /// This object as a "structurer" stereotype.
    /// </summary>
    [Export]
    public sealed class DomainObject
    {
        #region Fields 

        private readonly Subdomain1.IContract role;
        private readonly Subdomain2.IContract secondRole;
        private readonly externalLibrary.ExternalRole externalRole;

        #endregion

        #region Constructor

        [ImportingConstructor]
        public DomainObject(
            Subdomain1.IContract role,
            Subdomain2.IContract secondRole,
            externalLibrary.ExternalRole externalRole
            )
        {
            this.role = role;
            this.secondRole = secondRole;
            this.externalRole = externalRole;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Pass SecondRole PropertyTwo value in Role MethodOne and get Role PropertyOne.
        /// </summary>
        /// <returns>The value of Role PropertOne</returns>
        public string MethodThree()
        {
            string propertyTwo = this.secondRole.PropertyTwo;

            this.role.MethodOne(propertyTwo);

            return this.role.PropertyOne;
        }

        #endregion

    }
}
