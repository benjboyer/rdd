﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using rdd.tests.Subdomain1;

namespace rdd.tests
{
    [TestClass]
    public class Collaboration_ExternalTests
    {
        public Collaboration_ExternalTests()
        {
        }

        [TestMethod]
        public void Collaboration_External_Application2_NormalRun()
        {
            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer();

            var parameter = new externalLibrary.ExternalParameter("I'm Application2_NormalRun");

            var application2 = new synthax.Application(interfacer);
            application2.AddRole<Subdomain2.IContract>();
            application2.AddRole(parameter);

            var collaboration = new synthax.Collaboration(application2);
            collaboration.AllowsSharingFeature = true;

            //Now, each time you want to communicate with the object implementors of your roles, you must call these lines.
            var active = collaboration.Collaborate();
            var objectImplementor = active.GetObject<Subdomain2.IContract>();

            Assert.IsNotNull(objectImplementor);


            string result = objectImplementor.PropertyTwo;

            Assert.AreEqual("I'm Application2_NormalRun", result);
        }

        [TestMethod]
        public void Collaboration_External_Application2_AddExternalRole()
        {
            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer();

            var application2 = new synthax.Application(interfacer);
            application2.AddRole<Subdomain2.IContract>();
            application2.AddRole<externalLibrary.ExternalRole>();

            var collaboration = new synthax.Collaboration(application2);
            collaboration.AllowsSharingFeature = true;

            //Now, each time you want to communicate with the object implementors of your roles, you must call these lines.
            var active = collaboration.Collaborate();
            var objectImplementor = active.GetObject<Subdomain2.IContract>();

            Assert.IsNotNull(objectImplementor);


            string result = objectImplementor.PropertyTwo;

            Assert.AreEqual("Default Constructor Value", result);
        }
    }
}
