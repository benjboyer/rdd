﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using rdd.tests.Application1;

namespace rdd.tests
{
    [TestClass]
    public class CollaborationTests
    {
        public CollaborationTests()
        {
        }

        [TestMethod]
        public void RunningTests_Application1_CallKnowledgeProperty()
        {
            mef.MEFCatalog catalog = mef.MEFCatalog.Create()
                .AddType(typeof(Application1.ObjectImplementor))
                .AddType(typeof(Application1.IRoleProvider))
                .AddType(typeof(Application1.Injection));

            Injection injection = new Injection(" -injected");

            //At this point, specification calls should be only be called once.
            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer(catalog, injection);
            var application1 = new synthax.Application(interfacer);
            application1.AddRole(typeof(Application1.IRoleProvider));
            var collaboration = new synthax.Collaboration(application1);

            //Now, each time you want to communicate with the object implementors of your roles, you must call these lines.
            var active = collaboration.Collaborate();
            var objectImplementor = active.Role<Application1.IRoleProvider>();

            Assert.IsNotNull(objectImplementor);
            Assert.AreEqual("Default Application1", objectImplementor.MyRoleInformationAsKnowledge);

            objectImplementor.MyRoleInformationAsDoing("New message");

            Assert.AreEqual("New message -injected", objectImplementor.MyRoleInformationAsKnowledge);
        }

        [TestMethod]
        public void RunningTests_Application2_SharingApplication()
        {
            mef.MEFCatalog catalogApp1 = mef.MEFCatalog.Create()
                .AddType(typeof(Application1.ObjectImplementor))
                .AddType(typeof(Application1.Injection));

            mef.MEFCatalog catalogApp2 = mef.MEFCatalog.Create()
                .AddType(typeof(Application2.ObjectImplementor))
                .AddType(typeof(Application2.IRoleProvider));

            synthax.Interfacer interfacerApp1 = rdd.synthax.InterfacersFactory.CreateMEFInterfacer(catalogApp1, new Injection(" -injected"));

            synthax.Interfacer interfacerApp2 = rdd.synthax.InterfacersFactory.CreateMEFInterfacer(catalogApp2);

            var application1 = new synthax.Application(interfacerApp1);
            application1.AddRole(typeof(Application1.IRoleProvider));

            var application2 = new synthax.Application(interfacerApp2);
            application2.AddRole(typeof(Application2.IRoleProvider));

            var collaboration = new synthax.Collaboration();
            collaboration.AllowsSharingFeature = true;
            collaboration.Contract.AddApplication(application1);
            collaboration.Contract.AddApplication(application2);

            //Now, each time you want to communicate with the object implementors of your roles, you must call these lines.
            var active = collaboration.Collaborate();
            var objectImplementor = active.Role<Application2.IRoleProvider>();


            Assert.AreEqual("Default Application2", objectImplementor.MyRoleInformationAsKnowledge);

            Assert.IsNotNull(objectImplementor);

            objectImplementor.MyRoleInformationAsDoing("My message");

            Assert.AreEqual("My message Default Application1", objectImplementor.MyRoleInformationAsKnowledge );

        }
    }
}
