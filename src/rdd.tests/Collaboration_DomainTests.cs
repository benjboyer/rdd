﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using rdd.synthax;

namespace rdd.tests
{
    [TestClass]
    public class Collaboration_DomainTests
    {
        [TestMethod]
        public void Collaboration_Context_Run1()
        {   
            var ctx = new CollaborationContext<Domain1.DomainObject>()
                .Require(new Subdomain1.SubdomainParameter("Value"))
                .Require<externalLibrary.ExternalRole>();

            var domain = ctx.BuildContext();

            Assert.IsNotNull(domain);

        }
    }
}
