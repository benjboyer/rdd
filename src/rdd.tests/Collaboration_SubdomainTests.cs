﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using rdd.tests.Subdomain1;

namespace rdd.tests
{
    [TestClass]
    public class Collaboration_SubdomainTests
    {
        public Collaboration_SubdomainTests()
        {
        }

        [TestMethod]
        public void Collaboration_Subdomain1_ApplicationDefined_NormalRun()
        {
            SubdomainParameter injection = new SubdomainParameter("injected");

            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer();

            var application1 = new synthax.Application(interfacer);
            application1.AddRole<Subdomain1.IContract>();
            application1.AddRole(injection);

            var collaboration = new synthax.Collaboration(application1);
            collaboration.AllowsSharingFeature = true;

            //Now, each time you want to communicate with the object implementors of your roles, you must call these lines.
            var active = collaboration.Collaborate();
            var objectImplementor = active.GetObject<Subdomain1.IContract>();

            Assert.IsNotNull(objectImplementor);
            Assert.AreEqual(string.Empty, objectImplementor.PropertyOne);

            objectImplementor.MethodOne("New message");

            Assert.AreEqual("New message injected", objectImplementor.PropertyOne);
        }

        

        [TestMethod]
        public void Collaboration_Subdomain1_AddRolesOnContractDirectly()
        {
            SubdomainParameter injection = new SubdomainParameter("injected");

            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer();

            var collaboration = new synthax.Collaboration();
            collaboration.AllowsSharingFeature = true;
            collaboration.Contract.AddRole<Subdomain1.IContract>(interfacer);
            collaboration.Contract.AddRole(injection);

            var active = collaboration.Collaborate();
            var objectImplementor = active.GetObject<Subdomain1.IContract>();

            Assert.IsNotNull(objectImplementor);
        }

        [TestMethod]
        public void Collaboration_Subdomain1_AddApplicationOnContractDirectly()
        {
            SubdomainParameter injection = new SubdomainParameter("injected");

            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer();

            var application1 = new synthax.Application(interfacer);
            application1.AddRole<Subdomain1.IContract>();
            application1.AddRole(injection);

            var collaboration = new synthax.Collaboration();
            collaboration.AllowsSharingFeature = true;
            collaboration.Contract.AddApplication(application1);

            var active = collaboration.Collaborate();
            var objectImplementor = active.GetObject<Subdomain1.IContract>();

            Assert.IsNotNull(objectImplementor);
        }

        
    }
}
