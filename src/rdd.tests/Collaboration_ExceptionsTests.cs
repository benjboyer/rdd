﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using rdd.tests.Subdomain1;
using System.ComponentModel.Composition;

namespace rdd.tests
{
    [TestClass]
    public class Collaboration_ExceptionsTests
    {
        public Collaboration_ExceptionsTests()
        {
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Collaboration_Exceptions_Application_NullInterfacer()
        {
            SubdomainParameter injection = new SubdomainParameter("injected");
            var application1 = new synthax.Application(null);
        }

        [TestMethod]
        public void Collaboration_Exceptions_Application_AddSameRole()
        {
            SubdomainParameter injection = new SubdomainParameter("injected");
            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer();

            var application1 = new synthax.Application(interfacer);
            bool result = application1.AddRole<Subdomain1.IContract>(); //success
            bool result2 = application1.AddRole<Subdomain1.IContract>(); //failed

            Assert.IsTrue(result);
            Assert.IsFalse(result2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Collaboration_Exceptions_Collaboration_NullApplication()
        {
            SubdomainParameter injection = new SubdomainParameter("injected");
            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer();

            var application1 = new synthax.Application(interfacer);
            application1.AddRole<Subdomain1.IContract>();

            var collaboration = new synthax.Collaboration(null);

        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Collaboration_Exceptions_StaticContract_AddApplication()
        {
            SubdomainParameter injection = new SubdomainParameter("injected");
            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer();

            var application1 = new synthax.Application(interfacer);
            application1.AddRole<Subdomain1.IContract>();

            var collaboration = new synthax.Collaboration(application1);
            collaboration.Contract.AddApplication(application1);

        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Collaboration_Exceptions_StaticContract_AddRole()
        {
            SubdomainParameter injection = new SubdomainParameter("injected");
            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer();

            var application1 = new synthax.Application(interfacer);
            application1.AddRole<Subdomain1.IContract>();

            var collaboration = new synthax.Collaboration(application1);
            collaboration.Contract.AddRole<Subdomain1.IContract>(interfacer);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Collaboration_Exceptions_DynamicContract_AddNullApplication()
        {
            SubdomainParameter injection = new SubdomainParameter("injected");
            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer();

            var collaboration = new synthax.Collaboration();
            collaboration.Contract.AddApplication(null);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Collaboration_Exceptions_DynamicContract_AddRoleInterfacerNull()
        {
            SubdomainParameter injection = new SubdomainParameter("injected");
            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer();

            var collaboration = new synthax.Collaboration();
            collaboration.Contract.AddRole<Subdomain1.IContract>(null);

        }

        [TestMethod]
        public void Collaboration_Exceptions_DynamicContract_AddSameRoleFromApplication()
        {
            SubdomainParameter injection = new SubdomainParameter("injected");
            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer();

            var application1 = new synthax.Application(interfacer);
            application1.AddRole<Subdomain1.IContract>();

            var collaboration = new synthax.Collaboration();
            bool result1 = collaboration.Contract.AddApplication(application1); //success
            bool result2 = collaboration.Contract.AddRole<Subdomain1.IContract>(interfacer);  //failed

            Assert.IsTrue(result1);
            Assert.IsFalse(result2);
        }

        [TestMethod]
        public void Collaboration_Exceptions_DynamicContract_AddSameApplicationRoleFromRole()
        {
            SubdomainParameter injection = new SubdomainParameter("injected");
            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer();

            var application1 = new synthax.Application(interfacer);
            application1.AddRole<Subdomain1.IContract>();

            var collaboration = new synthax.Collaboration();
            bool result1 = collaboration.Contract.AddRole<Subdomain1.IContract>(interfacer); //success
            bool result2 = collaboration.Contract.AddApplication(application1); //failed
            
            Assert.IsTrue(result1);
            Assert.IsFalse(result2);
        }

        [TestMethod]
        public void Collaboration_Exceptions_ActiveCollaboration_NoDefaultNoRoles()
        {
            synthax.Collaboration collaboration = new synthax.Collaboration();

            using (var activeCollab = collaboration.Collaborate())
            {
                var role = activeCollab.GetObject<Subdomain1.IContract>();

                Assert.IsNull(role);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(CompositionException))]
        public void Collaboration_Exceptions_ActiveCollaboration_InjectionRequiredWithConstructor()
        {
            synthax.Interfacer interfacer = rdd.synthax.InterfacersFactory.CreateMEFInterfacer();

            var application1 = new synthax.Application(interfacer);
            application1.AddRole<Subdomain1.IContract>();

            var collaboration = new synthax.Collaboration(application1);
            collaboration.AllowsSharingFeature = true;

            var active = collaboration.Collaborate();
            var objectImplementor = active.GetObject<Subdomain1.IContract>();

        }
    }
}
