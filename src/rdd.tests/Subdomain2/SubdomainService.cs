﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.tests.Subdomain2
{
    internal class SubdomainService: IContract
    {
        #region Fields

        private readonly externalLibrary.ExternalRole externalRole;

        #endregion

        #region Properties

        public string PropertyTwo
        {
            get
            {
                return externalRole.PropertyThree;
            }
        }

        #endregion

        #region Constructor

        [ImportingConstructor]
        public SubdomainService(
                externalLibrary.ExternalRole externalRole
            )
        {
            this.externalRole = externalRole;
        }

        #endregion
    }
}
