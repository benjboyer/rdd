﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.tests.Subdomain2
{
    /// <summary>
    /// A simple contract defintion.
    /// This object is a "contract".
    /// </summary>
    [InheritedExport]
    public interface IContract
    {
        /// <summary>
        /// A second method in another contract/interface.
        /// </summary>
        string PropertyTwo { get; }
    }
}
