﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.tests.externalLibrary
{
    [Export]
    public sealed class ExternalParameter
    {
        #region Properties

        public string Parameter
        {
            get
            {
                return this._parameter;
            }
        }
        private readonly string _parameter;

        #endregion

        #region Constructors

        public ExternalParameter()
        {
            this._parameter = "Default Constructor Value";
        }
        
        public ExternalParameter(string parameter)
        {
            this._parameter = parameter;
        }

        #endregion

    }
}
