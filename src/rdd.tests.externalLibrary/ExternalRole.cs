﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;

namespace rdd.tests.externalLibrary
{
    /// <summary>
    /// An external simple contract.
    /// This object is a "contract".
    /// </summary>
    [InheritedExport]
    public interface ExternalRole
    {
        /// <summary>
        /// A property.
        /// </summary>
        string PropertyThree { get; }
    }
}
