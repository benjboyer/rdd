﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;

namespace rdd.tests.externalLibrary
{
    internal class ExternalObject : ExternalRole
    {
        #region Fields

        private ExternalParameter externalParameter;

        #endregion

        #region Properties

        public string PropertyThree
        {
            get
            {
                return this.externalParameter.Parameter;
            }
        }

        #endregion

        #region Constructor

        [ImportingConstructor]
        public ExternalObject(
                ExternalParameter externalParameter
            )
        {
            this.externalParameter = externalParameter;
        }

        #endregion
    }
}
