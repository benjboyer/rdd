﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace rdd.mef
{
    public class MEFContainer : IDisposable
    {
        #region Fields

        /// <summary>
        /// The aggregate catalog for this container.
        /// </summary>
        private readonly AggregateCatalog aggregateCatalog;

        /// <summary>
        /// A set of all exported interfaces.
        /// </summary>
        private readonly ISet<object> exportedImplementations;

        #endregion

        #region Constructor

        public MEFContainer(AggregateCatalog aggregateCatalog)
        {
            this.exportedImplementations = new HashSet<object>();
            this.aggregateCatalog = aggregateCatalog;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get the implementation of a service interface using MEF exporting attributes.
        /// </summary>
        /// <typeparam name="TService">The interface</typeparam>
        /// <returns>The implementation.</returns>
        /// <exception cref="System.ComponentModel.Composition.ImportCardinalityMismatchException"></exception>
        /// <exception cref="System.ObjectDisposedException"></exception>
        public TService GetService<TService>() where TService : class
        {
            var container = new CompositionContainer(aggregateCatalog);

            return this.getService<TService>(container);
        }

        /// <summary>
        /// Get the implementation of a service interface using MEF exporting attributes.
        /// </summary>
        /// <typeparam name="TService">The interface</typeparam>
        /// <returns>The implementation.</returns>
        /// <exception cref="System.ComponentModel.Composition.ImportCardinalityMismatchException"></exception>
        /// <exception cref="System.ObjectDisposedException"></exception>
        public TService GetService<TService>(params object[] instances) where TService : class
        {

            CompositionBatch batch = new CompositionBatch();

            foreach(var instance in instances)
            {
                batch.AddPart(instance);
            }

            var injectContainer = new CompositionContainer();
            injectContainer.Compose(batch);

            var mainCatalog = new CatalogExportProvider(this.aggregateCatalog);

            //Injected value are exported in first place, then the aggregate catalog.
            var mainContainer = new CompositionContainer(injectContainer, mainCatalog);
            
            mainCatalog.SourceProvider = mainContainer;

            return this.getService<TService>(mainContainer);
        }


        private TService getService<TService>(CompositionContainer container)
        {
            TService value = container.GetExport<TService>().Value;

            if (value != null && !this.exportedImplementations.Contains(value))
            {
                this.exportedImplementations.Add(value);
            }

            return value;
        }

        #endregion

        #region Implementation of IDisposable

        /// <summary>
        /// Dispose all the exported services.
        /// </summary>
        public void Dispose()
        {
            foreach (var service in this.exportedImplementations)
            {
                if (service is IDisposable)
                {
                    ((IDisposable)service).Dispose();
                }
                
            }
        }

        #endregion
    }
}
