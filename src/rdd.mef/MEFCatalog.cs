﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace rdd.mef
{
    public class MEFCatalog
    {
        #region Fields

        private AggregateCatalog catalog = null;

        #endregion

        #region Static accessors

        /// <summary>
        /// Create a new MEFCatalog builder instance.
        /// </summary>
        /// <returns>The MEFCatalog builder instance.</returns>
        public static MEFCatalog Create()
        {
            return new MEFCatalog();
        }

        #endregion

        #region Constructor

        private MEFCatalog()
        {
            catalog = new AggregateCatalog();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add an assembly into the catalog.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>The MEFCatalog builder instance.</returns>
        public MEFCatalog AddAssembly(Assembly assembly)
        {
            this.catalog.Catalogs.Add(new AssemblyCatalog(assembly));

            return this;
        }

        /// <summary>
        /// Add an assembly into the catalog with a specific reflection context.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="reflectionContext">A reflection context.</param>
        /// <returns>The MEFCatalog builder instance.</returns>
        public MEFCatalog AddAssembly(Assembly assembly, ReflectionContext reflectionContext)
        {
            this.catalog.Catalogs.Add(new AssemblyCatalog(assembly, reflectionContext));

            return this;
        }

        /// <summary>
        /// Add an type into the catalog.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The MEFCatalog builder instance.</returns>
        public MEFCatalog AddType(Type type)
        {
            this.catalog.Catalogs.Add(new TypeCatalog(type));

            return this;
        }

        /// <summary>
        /// Add many types into the catalog.
        /// </summary>
        /// <param name="types">The types.</param>
        /// <returns>The MEFCatalog builder instance.</returns>
        public MEFCatalog AddTypes(IEnumerable<Type> types)
        {
            this.catalog.Catalogs.Add(new TypeCatalog(types));

            return this;
        }

        /// <summary>
        /// Add many types into the catalog with a specific reflection context.
        /// </summary>
        /// <param name="types">The types.</param>
        /// <param name="reflectionContext">A reflection context.</param>
        /// <returns>The MEFCatalog builder instance.</returns>
        public MEFCatalog AddTypes(IEnumerable<Type> types, ReflectionContext reflectionContext)
        {
            this.catalog.Catalogs.Add(new TypeCatalog(types, reflectionContext));

            return this;
        }

        /// <summary>
        /// Add a directory into the catalog.
        /// </summary>
        /// <param name="directory">The folder of the directory</param>
        /// <returns>The MEFCatalog builder instance.</returns>
        public MEFCatalog AddDirectory(string directory)
        {
            this.catalog.Catalogs.Add(new DirectoryCatalog(directory));

            return this;
        }

        /// <summary>
        /// Add a directory into the catalog with a specific reflection context.
        /// </summary>
        /// <param name="directory">The folder of the directory</param>
        /// <param name="reflectionContext">A reflection context.</param>
        /// <returns>The MEFCatalog builder instance.</returns>
        public MEFCatalog AddDirectory(string directory, ReflectionContext reflectionContext)
        {
            this.catalog.Catalogs.Add(new DirectoryCatalog(directory, reflectionContext));

            return this;
        }

        /// <summary>
        /// Build an AggregateCatalog instance from the MEFCatalog builder instance.
        /// </summary>
        /// <returns>The AggregateCatalog instance.</returns>
        public AggregateCatalog Build()
        {
            return this.catalog;
        }

        #endregion
    }
}
