﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.synthax
{
    internal sealed class ObjectRole : BaseRole
    {
        #region Fields

        private readonly object _resolvedObject;

        #endregion

        #region Constructor

        public ObjectRole(object obj)
            : base(validateConstructor(obj))
        {
            this._resolvedObject = obj;
        }

        #endregion

        #region Implementation of abstract BaseRole

        public override object ResolveObject(ActiveCollaboration activeCollaboration, BaseRole initiator)
        {
            return this._resolvedObject;
        }

        #endregion

        #region Static accessors

        private static Type validateConstructor(object role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("Argument 'role' is null. Could not create a new ObjectRole.");
            }

            return role.GetType();
        }

        #endregion
    }
}
