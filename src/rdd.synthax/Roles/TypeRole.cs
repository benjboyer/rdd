﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.synthax
{
    internal sealed class TypeRole<TRoleType> : BaseRole
        where TRoleType : class
    {
        #region Properties

        /// <summary>
        /// The interfacer who provides the role object's implementation.
        /// </summary>
        public Interfacer Interfacer
        {
            get
            {
                return _interfacer;
            }
        }
        private readonly Interfacer _interfacer;

        #endregion

        #region Constructor

        public TypeRole(Interfacer interfacer)
            :base(typeof(TRoleType))

        {
            if (interfacer == null)
            {
                throw new ArgumentNullException("Argument 'interfacer' is null. Could not create a new TypeRole object.");
            }

            this._interfacer = interfacer;
        }

        #endregion

        #region Implementation of abstract BaseRole

        public override object ResolveObject(ActiveCollaboration activeCollaboration, BaseRole initiator)
        {
            return this.Interfacer.GetObject<TRoleType>(activeCollaboration, initiator);
        }

        #endregion
    }
}
