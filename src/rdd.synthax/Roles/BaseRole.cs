﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.synthax
{
    public abstract class BaseRole
    {
        #region Properties

        /// <summary>
        /// The type of this role.
        /// </summary>
        public Type Type
        {
            get
            {
                return this._type;
            }
        }
        private readonly Type _type;

        #endregion

        #region Constructor

        public BaseRole(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("Argument 'type' is null. Could not create a new BaseRole object.");
            }

            
            this._type = type;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Ask current interfacer to resole this role.
        /// </summary>
        /// <param name="activeCollaboration">The active collaboration.</param>
        /// <param name="initiator">The base role initiator (for recursivity).</param>
        /// <returns>The role resolved object.</returns>
        public abstract object ResolveObject(ActiveCollaboration activeCollaboration, BaseRole initiator);

        /// <summary>
        /// Determine if an object equals this role.
        /// To return a positive result, the object must equals to same role or same type (the property).
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is Type)
            {
                return this.Type.Equals((Type)obj);
            }

            if (obj is BaseRole)
            {
                BaseRole role = (BaseRole)obj;
                return this.Type.Equals(role.Type);
            }

            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return this.Type.GetHashCode();
        }

        #endregion
    }
}
