﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.synthax
{
    public sealed class CollaborationContext<TContext>
        where TContext : class
    {
        #region Fields

        private Collaboration collaboration;
        private Interfacer defaultInterfacer = new Interfacers.MEF.MEFInterfacer();

        #endregion

        #region Constructor

        /// <summary>
        /// Create a new collaboration context.
        /// </summary>
        public CollaborationContext()
        {
            this.collaboration = new Collaboration();
            this.collaboration.AllowsSharingFeature = true;
            this.collaboration.Contract.AddRole<TContext>(this.defaultInterfacer);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add a require value in the context.
        /// </summary>
        /// <param name="value">The required value.</param>
        /// <returns>The current collaboration context.</returns>
        public CollaborationContext<TContext> Require(object value)
        {
            this.collaboration.Contract.AddRole(value);

            return this;
        }

        /// <summary>
        /// Add a required contract in the context.
        /// This service will be resolved by the default interfacer [MEF].
        /// </summary>
        /// <typeparam name="TContract">The contract type.</typeparam>
        /// <returns>The current collaboration context.</returns>
        public CollaborationContext<TContext> Require<TContract>() where TContract : class
        {
            this.collaboration.Contract.AddRole<TContract>(this.defaultInterfacer);

            return this;
        }

        /// <summary>
        /// Add a required contract in the context.
        /// This service will be resolved by the specified interfacer.
        /// </summary>
        /// <typeparam name="TContract">The contract type.</typeparam>
        /// <param name="resolvedBy">The interfacer.</param>
        /// <returns>The current collaboration.</returns>
        public CollaborationContext<TContext> Require<TContract>(Interfacer resolvedBy) where TContract : class
        {
            this.collaboration.Contract.AddRole<TContract>(resolvedBy);

            return this;
        }

        /// <summary>
        /// Build a new context instance.
        /// </summary>
        /// <returns>The context instance.</returns>
        public TContext BuildContext()
        {
            var activeCollab = this.collaboration.Collaborate();

            return activeCollab.GetObject<TContext>();
        }

        #endregion
    }
}
;