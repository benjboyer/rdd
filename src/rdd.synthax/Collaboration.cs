﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.synthax
{
    public sealed class Collaboration
    {
        #region Properties

        /// <summary>
        /// Determines if this collaboration allows sharing feature between the interfacer.
        /// </summary>
        public bool AllowsSharingFeature { get; set; }

        /// <summary>
        /// Get or sets the default interfacer for this collaboration - for public collaboration.
        /// </summary>
        public Interfacer Default { get; set; }

        /// <summary>
        /// The contract of the collaboration.
        /// </summary>
        public Contract Contract { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a collaboration with an openned contrat - available for modifications.
        /// </summary>
        public Collaboration()
        {
            this.Contract = new Contract();
        }

        /// <summary>
        /// Creates a collaboration with the specify application.
        /// </summary>
        /// <param name="application">The application definition.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public Collaboration(Application application)
        {
            if (application == null)
            {
                throw new ArgumentNullException("Argument 'application' is null. Could not create a new collaboration.");
            }

            this.Contract = new Contract(application);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates an active collaboration.
        /// </summary>
        /// <exception cref="InvalidOperationException"></exception>
        /// <returns>The instance of a new active collaboration.</returns>
        public ActiveCollaboration Collaborate()
        {
            return new ActiveCollaboration(this);
        }

        #endregion
    }
}
