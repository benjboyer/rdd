﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.synthax
{
    public sealed class Contract
    {
        #region Fields

        private readonly bool _isDynamicContract;

        #endregion

        #region Properties

        /// <summary>
        /// Gets if this contract has predefined roles.
        /// </summary>
        public bool HasRoles
        {
            get
            {
                //Don't use variable "Count". Using FirstOrDefault takes less time and gives the same result.
                return this.Roles.FirstOrDefault() != null; 
            }
        }

        /// <summary>
        /// A list of all the type of the roles.
        /// </summary>
        internal IEnumerable<BaseRole> Roles
        {
            get
            {
                foreach(BaseRole role in _roles)
                    yield return role;

                foreach (Application application in _applications)
                    foreach (BaseRole role in application.Roles)
                        yield return role;
            }
        }

        private HashSet<Application> _applications;
        private HashSet<BaseRole> _roles;

        #endregion

        #region Constructors

        /// <summary>
        /// Create a new default contract.
        /// It's the only constructor that allows modifications on the contract.
        /// </summary>
        internal Contract()
            :this(true)
        {
            
        }

        /// <summary>
        /// Create a contract using a specific and exclusive application.
        /// Since it's exclusive, the contract can't be modified - you can't add any new role or application.
        /// </summary>
        /// <param name="application">The application.</param>
        internal Contract(Application application)
            : this(false) 
        {
            this._applications.Add(application);
        }

        private Contract(bool dynamique)
        {
            this._isDynamicContract = dynamique;
            _roles = new HashSet<BaseRole>();
            _applications = new HashSet<Application>();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds a new type role to the contract.
        /// </summary>
        /// <typeparam name="TRole">The known role type.</typeparam>
        /// <param name="interfacer">The interfacer used to resolve this role.</param>
        /// <exception cref="InvalidOperationException"></exception>
        /// <returns>True if added, false if failed.</returns>
        public bool AddRole<TRole>(Interfacer interfacer) where TRole : class
        {
            validateContractModificationAuthorization();

            if (interfacer == null)
            {
                throw new ArgumentNullException("Argument 'interfacer' is null. Could not add a new type role to contract.");
            }

            BaseRole newRole = new TypeRole<TRole>(interfacer);

            if (this.Roles.Contains(newRole))
            {
                return false;
            }

            _roles.Add(newRole);

            return true;
        }

        /// <summary>
        /// Adds a new object role to the contract.
        /// </summary>
        /// <param name="role">The object to add.</param>
        /// <param name="interfacer">The interfacer used to resolve this role.</param>
        /// <exception cref="InvalidOperationException"></exception>
        /// <returns>True if added, false if failed.</returns>
        public bool AddRole(object role)
        {
            validateContractModificationAuthorization();

            BaseRole newRole = new ObjectRole(role);

            if (this.Roles.Contains(newRole))
            {
                return false;
            }

            _roles.Add(newRole);

            return true;
        }

        /// <summary>
        /// Adds a new application to the contract.
        /// </summary>
        /// <param name="application">The application.</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        /// <returns>True if added, false if failed.</returns>
        public bool AddApplication(Application application)
        {
            validateContractModificationAuthorization();

            if (application == null)
            {
                throw new ArgumentNullException("Argument 'application' is null. Could not add a new application to contract.");
            }

            if (_applications.Contains(application))
            {
                return false;
            }

            foreach(var role in application.Roles)
            {
                if (this.Roles.Contains(role))
                {
                    return false;
                }
            }

            _applications.Add(application);

            return true;
        }

        private void validateContractModificationAuthorization()
        {
            if (!_isDynamicContract)
            {
                throw new InvalidOperationException("The contract cannot change from his initialization state. Use the default constructor. Validation failed.");
            }
        }

        #endregion
    }
}
