﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.synthax
{
    public sealed class ActiveCollaboration : IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the current collaboration.
        /// </summary>
        internal Collaboration Collaboration
        {
            get
            {
                return this._collaboration;
            }
        }
        private readonly Collaboration _collaboration;

        /// <summary>
        /// Gets the current active collaboration session.
        /// </summary>
        internal ActiveCollaborationSessionCollection Session
        {
            get
            {
                return this._session;
            }
        }
        private readonly ActiveCollaborationSessionCollection _session;

        #endregion

        #region Constructor

        internal ActiveCollaboration(Collaboration collaboration)
        {
            if (collaboration == null)
            {
                throw new ArgumentNullException("Argument 'collaboration' is null. Could not create a new active collaboration");
            }

            this._collaboration = collaboration;
            this._session = new ActiveCollaborationSessionCollection();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get the resolvable object from a specific role.
        /// </summary>
        /// <typeparam name="TRole">The role type.</typeparam>
        /// <returns>The object (or implementation) of the role.</returns>
        public TRole GetObject<TRole>() where TRole : class
        {
            if (this._collaboration.Contract.HasRoles)
            {
                BaseRole foundRole = this._collaboration.Contract.Roles.FirstOrDefault((role) => role.Equals((typeof(TRole))));
                if (foundRole == null)
                {
                    throw new ArgumentException(String.Format("The specified role '{0}' is not defined for this active collaboration.", typeof(TRole).Name));
                }

                var result = foundRole.ResolveObject(this, foundRole) as TRole;

                if (result == null)
                {
                    throw new InvalidOperationException(string.Format("Result of method ResolveObject of current role '{0}' returned an invvalid value. Could not get object.", foundRole.GetType().FullName));
                }

                return result;
            }
            else if (this._collaboration.Default != null)
            {
                BaseRole callingRole = new TypeRole<TRole>(this._collaboration.Default);

                var result = callingRole.ResolveObject(this, callingRole) as TRole;

                if (result == null)
                {
                    throw new InvalidOperationException(string.Format("Result of method ResolveObject of current role '{0}' returned an invvalid value. Could not get object.", callingRole.GetType().FullName));
                }

                return result;
            }

            return default(TRole);
        }

        #endregion

        #region Implementation of IDisposable

        public void Dispose()
        {
            this.Session.Clear();
        }

        #endregion
    }
}
