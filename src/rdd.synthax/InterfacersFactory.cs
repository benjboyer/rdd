﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using rdd.mef;
using System.Reflection;
using rdd.synthax.Interfacers.WCF;

namespace rdd.synthax
{
    public static class InterfacersFactory
    {
        #region MEF

        public static Interfacer CreateMEFInterfacer()
        {
            return new Interfacers.MEF.MEFInterfacer();
        }

        #endregion

        #region WCF

        public static Interfacer CreateWCFInterfacer(WCFConfiguration wcfConfiguration)
        {
            return new Interfacers.WCF.WCFInterfacer(wcfConfiguration);
        }


        #endregion
    }
}
