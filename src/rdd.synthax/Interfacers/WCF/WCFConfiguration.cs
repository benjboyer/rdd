﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace rdd.synthax.Interfacers.WCF
{
    public sealed class WCFConfiguration
    {
        private Binding m_binding = null;

        private string m_endpoint = string.Empty;

        private Configuration m_configuration = null;

        /// <summary>
        /// Gets or sets the remote address used by the channel. Null by default.
        /// </summary>
        public string RemoteAddress { get; set; }

        public IReadOnlyList<MessageHeader> Headers
        {
            get
            {
                return m_headers;
            }
        }
        private List<MessageHeader> m_headers = new List<MessageHeader>();

        public WCFConfiguration(Binding binding)
        {
            this.m_binding = binding;
        }

        public WCFConfiguration(string endpoint)
        {
            this.m_endpoint = endpoint;
        }

        public WCFConfiguration(string endpoint, Configuration configuration)
        {
            this.m_endpoint = endpoint;
            this.m_configuration = configuration;
        }

        public ChannelFactory<TChannel> GetFactory<TChannel>()
        {
            ChannelFactory<TChannel> factory = null;

            if (this.m_binding != null)
            {
                if(!String.IsNullOrEmpty(this.RemoteAddress))
                {
                    factory = new ChannelFactory<TChannel>(this.m_binding, new EndpointAddress(this.RemoteAddress));
                }
                else
                {
                    factory = new ChannelFactory<TChannel>(this.m_binding);
                }
            }
            else
            {
                if (this.m_configuration != null)
                {
                    if (!String.IsNullOrEmpty(this.RemoteAddress))
                    {
                        factory = new ConfigurationChannelFactory<TChannel>(this.m_endpoint, this.m_configuration, new EndpointAddress(this.RemoteAddress));
                    }
                    else
                    {
                        factory = new ConfigurationChannelFactory<TChannel>(this.m_endpoint, this.m_configuration, null);
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(this.RemoteAddress))
                    {
                        factory = new ChannelFactory<TChannel>(this.m_endpoint, new EndpointAddress(this.RemoteAddress));
                    }
                    else
                    {
                        factory = new ChannelFactory<TChannel>(this.m_endpoint);
                    }
                }
            }


            return factory;
        }
    
        public void AddHeader(MessageHeader header)
        {
            if (header == null)
            {
                throw new ArgumentNullException("Argument 'header' is null. Could not add header to WCFConfiguration.");
            }

            bool contained = this.Headers.FirstOrDefault((h) => h.Name == header.Name) != null;

            if (contained)
            {
                throw new InvalidOperationException(string.Format("Message header '{0}' already exist. Could not add header to WCFConfiguration.", header.Name));
            }

            this.m_headers.Add(header);
        }
    
    }
}
