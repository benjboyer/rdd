﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace rdd.synthax.Interfacers.WCF
{
    internal class WCFInterfacer : Interfacer
    {
        #region Fields

        private readonly WCFConfiguration configuration;

        #endregion

        #region Constructors

        public WCFInterfacer(WCFConfiguration configuration)
        {
            this.configuration = configuration;
        }

        #endregion

        #region Implementation of Interfacer

        public TRole GetObject<TRole>(ActiveCollaboration activeCollaboration, BaseRole initiator) where TRole : class
        {
            var factory = this.configuration.GetFactory<TRole>();
            var channel = factory.CreateChannel();

            if (this.configuration.Headers.Count > 0)
            {
                using (var scope = new OperationContextScope((IContextChannel)channel))
                {
                    foreach (var header in this.configuration.Headers)
                    {
                        OperationContext.Current.OutgoingMessageHeaders.Add(header);
                    }
                }
            }

            return channel;
        }

        #endregion
    }
}
