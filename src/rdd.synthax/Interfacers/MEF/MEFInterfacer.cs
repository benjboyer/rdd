﻿using rdd.mef;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace rdd.synthax.Interfacers.MEF
{
    internal class MEFInterfacer : Interfacer
    {
        #region Constructor

        public MEFInterfacer()
        {
        }

        #endregion

        #region Implementation of Interfacer

        public TRole GetObject<TRole>(ActiveCollaboration activeCollaboration, BaseRole initiator) where TRole : class
        {
            if (activeCollaboration == null)
            {
                throw new ArgumentNullException("Argument 'activeCollaboration' is null and required. Could not GetObject from MEFInterfacer.");
            }

            if (initiator == null)
            {
                throw new ArgumentNullException("Argument 'initiator' is null and required. Could not GetObject from MEFInterfacer.");
            }

            MEFCatalog catalog = null;

            //We make sure the MEFCatalog is build once per ActiveCollaboration.
            if (!activeCollaboration.Session.ContainsKey("catalog"))
            {
                catalog = getConcernedAssembliesCatalog(activeCollaboration);
                activeCollaboration.Session.Add("catalog", catalog);
            }
            else //If it's already built, we take it from the session.
            {
                catalog = activeCollaboration.Session["catalog"] as MEFCatalog;
            }


            MEFContainer container = new MEFContainer(catalog.Build());

            bool allowSharing = activeCollaboration.Collaboration.AllowsSharingFeature;

            if (allowSharing) //Feature must be enabled.
            {
                Type tType = typeof(TRole);

                IList<object> newInjections = new List<object>();

                foreach (var role in activeCollaboration.Collaboration.Contract.Roles)
                {
                    if (!role.Type.Equals(tType) && //Prevent from calling itself. 
                        role != initiator) //Is not the owner of this collaboration session. Prevent infinite loop from other interfacers.
                    {
                        object instance = role.ResolveObject(activeCollaboration, initiator);
                        object wrapper = createWrapper(instance);
                        newInjections.Add(wrapper);
                    }
                }

                if (newInjections.Count > 0)
                {
                    return container.GetService<TRole>(newInjections.ToArray());
                }
                else
                {
                    return container.GetService<TRole>();
                }
            }
            else
            {
                return container.GetService<TRole>();
            }
        }

        #endregion

        #region Methods

        private object createWrapper(object instance)
        {
            var d1 = typeof(MEFWrapper<>);
            Type[] typeArgs = { instance.GetType() };
            var makeme = d1.MakeGenericType(typeArgs);

            object[] args = { instance };

            return Activator.CreateInstance(makeme, args);
        }


        private MEFCatalog getConcernedAssembliesCatalog(ActiveCollaboration activeCollaboration)
        {
            var catalog = MEFCatalog.Create();

            HashSet<Assembly> assemblies = new HashSet<Assembly>();

            foreach (var role in activeCollaboration.Collaboration.Contract.Roles)
            {
                Assembly roleAssembly = role.Type.Assembly;

                if (!assemblies.Contains(roleAssembly))
                {
                    assemblies.Add(roleAssembly);

                    catalog.AddAssembly(roleAssembly);
                }
            }

            return catalog;
        }

        #endregion

    }

    class MEFWrapper<TWrappedService>
        where TWrappedService : class
    {
        #region Fields

        [Export]
        private TWrappedService Service;

        #endregion

        #region Constructor

        public MEFWrapper(TWrappedService Service)
        {
            this.Service = Service;
        }

        #endregion
    }
}
