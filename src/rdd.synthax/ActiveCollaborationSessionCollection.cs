﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.synthax
{
    class ActiveCollaborationSessionCollection : NameObjectCollectionBase
    {
        public ActiveCollaborationSessionCollection()
        {
        }


        public DictionaryEntry this[int index]
        {
            get
            {
                return (new DictionaryEntry(
                    this.BaseGetKey(index), this.BaseGet(index)));
            }
        }

        public object this[string key]
        {
            get
            {
                return (this.BaseGet(key));
            }
            set
            {
                this.BaseSet(key, value);
            }
        }

        public string[] AllKeys
        {
            get
            {
                return (this.BaseGetAllKeys());
            }
        }

        public Array AllValues
        {
            get
            {
                return (this.BaseGetAllValues());
            }
        }

        public bool HasKeys
        {
            get
            {
                return (this.BaseHasKeys());
            }
        }

        public bool ContainsKey(string key)
        {
            return this.BaseGet(key) != null;
        }

        public void Add(string key, object value)
        {
            this.BaseAdd(key, value);
        }

        public void Remove(string key)
        {
            this.BaseRemove(key);
        }

        public void Remove(int index)
        {
            this.BaseRemoveAt(index);
        }

        public void Clear()
        {
            this.BaseClear();
        }

    }
}


