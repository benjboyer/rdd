﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.synthax
{
    public interface Interfacer
    {
        /// <summary>
        /// Allows to get the object associated to a role based on an active collboration provided and its interfacer.
        /// </summary>
        /// <typeparam name="TRole">The role type.</typeparam>
        /// <param name="activeCollaboration">The active collboration who made the request.</param>
        /// <param name="initiator">The base role initator.</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="NullReferenceException"></exception>
        /// <returns>The object (or implementation) of the role.</returns>
        TRole GetObject<TRole>(ActiveCollaboration activeCollaboration, BaseRole initiator) where TRole : class;
    }
}
