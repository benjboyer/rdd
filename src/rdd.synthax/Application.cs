﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rdd.synthax
{
    /// <summary>
    /// Create an application who can scope roles to a specific application.
    /// </summary>
    public sealed class Application
    {
        #region Fields

        private readonly Interfacer _interfacer;

        #endregion

        #region Properties

        /// <summary>
        /// Roles of the application.
        /// </summary>
        internal IEnumerable<BaseRole> Roles
        {
            get
            {
                return _roles;
            }
        }

        private HashSet<BaseRole> _roles;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new application with its interfacer.
        /// </summary>
        /// <param name="interfacer">The interfacer of this application.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public Application(Interfacer interfacer)
        {
            if (interfacer == null)
            {
                throw new ArgumentNullException("Argument 'interfacer' is null. Could not create a new application.");
            }
            this._roles = new HashSet<BaseRole>();
            this._interfacer = interfacer;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add a new type role to the application.
        /// </summary>
        /// <typeparam name="TRole">The type of the role - Must be an interface.</typeparam>
        /// <exception cref="ArgumentNullException"></exception>
        /// <returns>True if added, false if failed.</returns>
        public bool AddRole<TRole>() where TRole : class
        {
            BaseRole newRole = new TypeRole<TRole>(this._interfacer);

            if (_roles.Contains(newRole))
            {
                return false;
            }

            _roles.Add(newRole);

            return true;
        }

        /// <summary>
        /// Add a new object role to the application.
        /// </summary>
        /// <param name="obj">The object to add.</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <returns>True if added, false if failed.</returns>
        public bool AddRole(object obj)
        {
            BaseRole newRole = new ObjectRole(obj);

            if (_roles.Contains(newRole))
            {
                return false;
            }

            _roles.Add(newRole);

            return true;
        }

        #endregion
    }
}
